#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import pprint
import re
import sre_constants
import sys
import yaml

# Version string
VERSION = "2.7.1"

# Global switch to be verbose
VERBOSE = False

# Default Error return value
RETURN_DEFAULT = 1

# Return value if acronyms were found
RETURN_ACRONYMS = 2

# Return value if patterns were unused
RETURN_PATTERNS = 4


def main():
    global VERBOSE
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--no-headings", action="store_true", help="Ignore headings while checking.")
    parser.add_argument("--no-captions", action="store_true", help="Ignore captions while checking.")
    parser.add_argument("-c", "--config", default=".acrofind.yml", help="Configuration file to load.")
    parser.add_argument("-i", "--show-ignored", action="store_true", help="Show ignored lines.")
    parser.add_argument("-l", "--line", type=int, help="Only process this line number.")
    parser.add_argument("-s", "--show", action="store_true", help="Show loaded acronyms.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Be more verbose.")
    parser.add_argument("-V", "--version", action="store_true", help="Show version information and exit.")
    args = parser.parse_args()

    if args.version:
        print("Acrofind v{}".format(VERSION))
        sys.exit(0)

    if args.verbose:
        VERBOSE = True

    # load config file or exit
    if os.path.exists(args.config):
        with open(args.config) as f:
            config = yaml.load(f, Loader=yaml.Loader)
    else:
        print("Can not find \"{}\".".format(args.config))
        sys.exit(RETURN_DEFAULT)

    # check for empty configuration
    if not config:
        print("No configuration to read.")
        sys.exit(RETURN_DEFAULT)

    if args.verbose:
        pprint.pprint(config)
        print()

    # check whether file with acronyms is defined in config
    if "acrofile" not in config:
        print("Config has no entry \"acrofile\".")
        sys.exit(RETURN_DEFAULT)

    # load case-sensitive acronyms from config
    case_sensitive = None
    if "case-sensitive" in config:
        case_sensitive = config["case-sensitive"]

    ignore = None
    if "ignore" in config:
        ignore = config["ignore"]

    # load acronyms
    try:
        acronyms = load_acronyms(config["acrofile"], case_sensitive, ignore)
    except sre_constants.error as e:
        print(e)
        sys.exit(RETURN_DEFAULT)

    if args.verbose:
        print("Found {} acronyms.".format(len(acronyms)))

    if len(acronyms) == 0:
        print("No acronyms to check.")
        sys.exit(RETURN_DEFAULT)

    # show acronyms
    if args.show:
        for a in acronyms:
            print(a)

    # check whether texfiles are defined
    if "texfiles" not in config:
        print("Config has no entry \"texfiles\".")
        sys.exit(RETURN_DEFAULT)

    # go through texfiles
    c = 0
    for file in config["texfiles"]:
        c += process_file(file, acronyms, not args.no_headings, not args.no_captions, args.show_ignored, args.line)

    print("Found {} unwrapped acronym{}.".format(c, "" if c == 1 else "s"))

    return_value = 0
    if c > 0:
        return_value += RETURN_ACRONYMS

    output = "\n"
    output += "Unused ignore patterns:\n"
    found = False
    for acronym in acronyms:
        unused = acronym.get_unused_ignore_patters()
        if len(unused) > 0:
            output += "  - {}: {}\n".format(acronym.short, [p.pattern for p in unused])
            found = True

    if found:
        print(output)
        return_value += RETURN_PATTERNS

    sys.exit(return_value)


def process_file(file, acronyms, check_headings, check_captions, show_ignored, linenumber):
    """Check a tex file for appearances of acronyms not surrounded by a latex command

    Parameters
    ----------
    file : str
        File to check.
    acronyms : list
        List of acronyms to check
    check_headings : bool
        Whether to check headings.
    check_captions : bool
        Whether to check captions.
    show_ignored : bool
        Whether to print ignored lines.
    linenumber : int
        Process only the given line number.

    Returns
    -------
    int
        Number of acronyms found.
    """

    # pattern to exclude section headers
    headings = re.compile(r"\\((sub){0,2}section|paragraph|chapter){.*}")

    caption = re.compile(r"\\caption{.*}")

    # pattern to exclude lines that are comments
    comment = re.compile(r"\s*%")

    c = 0
    try:
        with open(file) as f:
            for nr, line in enumerate(f.readlines()):
                if not linenumber or linenumber == nr + 1:
                    for acronym in acronyms:
                        if not (not check_headings and headings.search(line)) and \
                                not (not check_captions and caption.search(line)) and \
                                not comment.match(line):
                            if acronym.search(line):
                                print("\033[35m{}:\033[32m{} \033[33m{}\033[0m\n{}\n".format(
                                    file, nr + 1, acronym.short,
                                    line.strip()))
                                c += 1
                            if show_ignored and acronym.line_was_ignored:
                                print("\033[35m{}:\033[32m{} \033[33m{}\n\033[31mIGNORE:\033[0m\t{}\n".format(
                                    file, nr + 1, acronym.short, line.strip()))
    except FileNotFoundError:
        print("\033[31mCan not find file \"{}\".\033[0m".format(file))
        sys.exit(RETURN_DEFAULT)
    return c


def load_acronyms(file, case_sensitive, ignore):
    """Load acronyms from file. Currently finds only acronyms defined with \acro{acronym}[short]{full}.

    Parameters
    ----------
    file : str
        Texfile to load acronyms from.
    case_sensitive : list
        List of case-sensitive acronyms.
    ignore : list
        List of patterns to ignore lines per acronym.

    Returns
    -------
    list
        List of acronyms extracted from file.
    """
    acronyms = []
    pattern = re.compile(r"\\acro{(.*)}\[(.*)\]{(.*)}")
    comment = re.compile(r"^\s*%")
    with open(file) as f:
        for line in f.readlines():
            if not comment.match(line):
                match = pattern.search(line)
                if match:
                    acro = match.group(1)
                    short = match.group(2)
                    full = match.group(3)
                    ignore_patterns = None
                    if ignore is not None:
                        if short in ignore:
                            ignore_patterns = ignore[short]
                    acronyms.append(Acronym(acro, short, full,
                                            case_sensitive is not None and short in case_sensitive, ignore_patterns))
    return acronyms


class Acronym:

    """Class to hold an acronym.
    """

    def __init__(self, acronym, short, full, case_sensitive=False, ignore=None):
        """Construct an acronym.

        Parameters
        ----------
        acronym : str
            Shortcut used in the tex document.
        short : str
            Short version of the acronym.
        full : str
            Long version of the acronym.
        case_sensitive : bool, optional
            Whether the search of this acronym should be case-sensitive.
        ignore : list, optional
            List of patterns to ignore lines for this acronym.
        """

        self._acronym = acronym
        self._short = short.strip()
        self._full = re.sub(r"\\acroextra{.*}", '', full).strip()
        self._case_sensitive = case_sensitive

        # Precompile ignore patterns if given
        self._external_ignore = []
        self._used_external_ignore = []
        if ignore is not None:
            for pattern in ignore:
                self._external_ignore.append(re.compile(pattern))
                self._used_external_ignore.append(False)

        # list of patterns that depend on this acronym
        self._internal_ignore = []
        self._internal_ignore.append(re.compile(r"\\[Cc]ref{[^}]*" + short.lower() + r"[^}]*}"))
        self._internal_ignore.append(re.compile(r"\\labelcref{[^}]*" + short.lower() + r"[^}]*}"))
        self._internal_ignore.append(re.compile(r"\\label{[^}]*" + short.lower() + r"[^}]*}"))
        self._internal_ignore.append(re.compile(r"\\ac[slfp\*]*{" + acronym + r"}"))

        # Precompile search pattern based on case-sensitivity.
        needle = short.lower()
        if case_sensitive:
            needle = short
        self._pattern_short = re.compile(r"\b" + needle + r"\b")

        needle = full.lower()
        if case_sensitive:
            needle = full
        self._pattern_full = re.compile(needle)

    def search(self, line):
        """Search a line for this acronym.
        Applies ignore patterns to the line.

        Parameters
        ----------
        line : str
            Line to search through.

        Returns
        -------
        bool
            Whether this acronym was found in the given line respecting ignore patterns.
        """

        if VERBOSE:
            print("Acronym[{}].search()".format(self._short))

        self._line_was_ignored = False

        if VERBOSE:
            print("\tLine: {}".format(line.strip()))

        # save whether the line matched before applying ignore patterns
        if self._case_sensitive:
            before = (self._pattern_short.search(line) or self._pattern_full.search(line)) is not None
        else:
            before = (self._pattern_short.search(line.lower()) or self._pattern_full.search(line.lower())) is not None

        if not before:
            return False

        for pattern in self._internal_ignore:
            line = re.sub(pattern, '', line)

        for index, pattern in enumerate(self._external_ignore):
            if pattern.search(line):
                line = re.sub(pattern, '', line)
                self._used_external_ignore[index] = True

        if not self._case_sensitive:
            line = line.lower()

        after = (self._pattern_short.search(line) or self._pattern_full.search(line)) is not None

        self._line_was_ignored = before and not after

        if VERBOSE:
            print("\tbefore: {}".format(before))
            print("\tafter: {}".format(after))
            print("\tLine: {}".format(line.strip()))
            print("\tignored: {}".format(self._line_was_ignored))

        return after

    def get_unused_ignore_patters(self):
        """List all patterns not used to ignore a line so far.

        Returns
        -------
        list
            List of patterns not used to ignore a line so far.
        """
        result = []
        for index, pattern in enumerate(self._external_ignore):
            if not self._used_external_ignore[index]:
                result.append(pattern)
        return result

    @property
    def acronym(self):
        return self._acronym

    @property
    def short(self):
        return self._short

    @property
    def full(self):
        return self._full

    @property
    def line_was_ignored(self):
        return self._line_was_ignored

    @property
    def case_sensitive(self):
        return self._case_sensitive

    def __str__(self):
        return "{}\t{}\t{}\t{}".format(self._acronym, self._short, self._case_sensitive, self._full)


if __name__ == '__main__':
    main()
