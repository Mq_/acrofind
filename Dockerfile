FROM python:3.5-slim
LABEL maintainer="mh@0x25.net"

WORKDIR /usr/src/app

COPY acrofind.py requirements.txt LICENSE ./
RUN pip install -r requirements.txt

ENV PATH="/usr/src/app:${PATH}"

CMD ["acrofind.py"]
