# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Add support for \labelcref.

### Changed

- Improve output of acronyms.

## [2.7.1] - 2020-09-30

### Added

- Remove \acroextra in full name of acronym.
- Print acronym for ignored lines.

### Fixed

- Fix missing starred versions of \ac in ignore pattern.

## [2.7.0] - 2020-08-21

### Added

- Add switch (-l) to process only one line number.

### Changed

- Use better variable names in acronym class.

### Fixed

- Fix patterns for references, labels and acronyms.

## [2.6.0] - 2020-08-20

### Added

- Add switch (-i) to show ignored lines.

### Changed

- Remove ignore patterns from line before check to detect other occurrences in the same line.

### Fixed

- Fix not detecting acronyms at beginning of line.

## [2.5.1] - 2020-08-11

### Fixed

- Fix ingoring commented \acro lines.

## [2.5.0] - 2020-08-11

### Added

- Add Dockerfile and build for Docker Hub.
- Ignore commented \acro lines.
- Ignore acronyms in \label and \cref / \ref.

### Fixed

- Fix requirements.txt

## [2.4.0] - 2020-07-31

### Added

- Use grep-style colors.
- Catch error while parsing patterns.

### Changed

- Improve short regex with word boundaries.

## [2.3.0] - 2020-07-06

### Added

- Quit with message, if no acronyms to check can be found.
- Add switch `--no-headings` to ignore headings (chapters, sections, and paragraphs).
- Add switch `--no-captions` to ignore captions.

### Changed

- Display unused ignore patterns only if at least one is found.
- Check for headings and captions by default.

## [2.2.0] - 2020-06-05

### Added

- Also look for full version of acronym.

### Removed

- Remove unused getters.

### Fixed

- Fix catching empty configuration file.

## [2.1.0] - 2020-05-23

### Added

- Add minimal working example.
- Add error handling for FileNotFoundError.
- Add option to show version information.
- Add return values for general error, unwrapped acronym and unused pattern.
- Add ability to change configuration file by command line parameter.

## [2.0.0] - 2020-05-11

### Added

- Show count of found acronyms.
- Add possibility to ignore unwrapped acronyms per acronym based on patterns.
- Show ignore acronym patterns that were not used.
- Add verbosity switch.

### Removed

- Remove support for associative arrays of texfiles to process (was: `- name: <filename>`, now: `- <filename>`).
  Support for more attributes per file is unlikely to be required in the future.

## [1.0.0] - 2020-05-06

### Added

- Read acronyms from `\acro` command in a single file.
- Go through list of of tex files and look for acronyms not wrapped in command.
- Turn of case-sensitivity for predefined acronyms.

[Unreleased]: https://gitlab.com/Mq_/acrofind/compare/v2.7.1...master
[2.7.1]: https://gitlab.com/Mq_/acrofind/compare/v2.7.0...v2.7.1
[2.7.0]: https://gitlab.com/Mq_/acrofind/compare/v2.6.0...v2.7.0
[2.6.0]: https://gitlab.com/Mq_/acrofind/compare/v2.5.1...v2.6.0
[2.5.1]: https://gitlab.com/Mq_/acrofind/compare/v2.5.0...v2.5.1
[2.5.0]: https://gitlab.com/Mq_/acrofind/compare/v2.4.0...v2.5.0
[2.4.0]: https://gitlab.com/Mq_/acrofind/compare/v2.3.0...v2.4.0
[2.3.0]: https://gitlab.com/Mq_/acrofind/compare/v2.2.0...v2.3.0
[2.2.0]: https://gitlab.com/Mq_/acrofind/compare/v2.1.0...v2.2.0
[2.1.0]: https://gitlab.com/Mq_/acrofind/compare/v2.0.0...v2.1.0
[2.0.0]: https://gitlab.com/Mq_/acrofind/compare/v1.0.0...v2.0.0
[1.0.0]: https://gitlab.com/Mq_/acrofind/-/tags/v1.0.0
