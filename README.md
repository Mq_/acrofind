# Acrofind

Goes through Tex files and looks for occurrences of defined acronyms that are written in plaintext instead of being wrapped by the acronym command.
For example, if you defined `\acro{it}[IT]{Information Technology}` and forgot to write `\acro{it}` and wrote `IT` instead.

Find the repository at [GitLab](https://gitlab.com/Mq_/acrofind).

## Limitations

- Currently only works with the `acronym` package that defines acronyms with `\acro{acronym}[short]{full}` in a single file.

## Installation

Place the location of `acrofind.py` in your `$PATH`.

## Execution

Make sure `.acrofind.yml` exists in the current working directory and the relative paths configured are reachable.
Then, run:

```
acrofind.py
```

Run `acrofind.py -h` for command line options.

## Configuration

Create a YAML file `.acrofind.yml`, typically where your LaTeX root document resides.
You can configure the file that holds all the acronyms, all files to check for occurrences of the acronyms, and a list of acronyms that are treated case-sensitive.

```
acrofile: <file where acronyms are defined>
texfiles:
  - <file to search>
  - ...
case-sensitive:
  - <list of acronyms treated case-sensitive>
  - ...
ignore:
  <acronym>:
    - <pattern1>
    - <pattern2>
    - ...
```

A minimal working example can be found in [example](example).
Ignore patterns are remove from each line before trying to detect an acronym that is not wrapped in the acronym command.

## Return Values

Program returns the sum of the following values:

- `0` if all went well.
- `1` for general errors.
- `2` if at least one unwrapped acronym was found.
- `4` if at least one ignore pattern for a defined acronym was not used.

## Todo

- Add support for `glossaries` package.
- Detect defined but unused acronyms.
- Allow for acronym definitions with missing short version (`\acro{acronym}{full}`).
